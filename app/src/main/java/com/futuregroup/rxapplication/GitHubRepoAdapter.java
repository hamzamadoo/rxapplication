package com.futuregroup.rxapplication;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.futuregroup.rxapplication.databinding.ItemGithubRepoBinding;

import java.util.ArrayList;
import java.util.List;

public class GitHubRepoAdapter extends BaseAdapter {

    private List<GitHubRepo> gitHubRepos = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public GitHubRepoAdapter(Context context){
        layoutInflater=LayoutInflater.from(context);
    }

    @Override public int getCount() {
        return gitHubRepos.size();
    }

    @Override public GitHubRepo getItem(int position) {
        if (position < 0 || position >= gitHubRepos.size()) {
            return null;
        } else {
            return gitHubRepos.get(position);
        }
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        final View view = (convertView != null ? convertView : createView(parent).getRoot());
        final GitHubRepoViewHolder viewHolder = (GitHubRepoViewHolder) view.getTag();
        viewHolder.repoBinding.setModel(getItem(position));
        return view;
    }

    public void setGitHubRepos(@Nullable List<GitHubRepo> repos) {
        if (repos == null) {
            return;
        }
        gitHubRepos.clear();
        gitHubRepos.addAll(repos);
        notifyDataSetChanged();
    }

    public List<GitHubRepo> getGitHubRepos(){
        return gitHubRepos;
    }

    private ItemGithubRepoBinding createView(ViewGroup parent) {

//       ItemGithubRepoBinding itemGithubRepoBinding=ItemGithubRepoBinding.inflate(layoutInflater,parent,false);
        ItemGithubRepoBinding itemGithubRepoBinding= DataBindingUtil.inflate(layoutInflater,R.layout.item_github_repo,parent,false);
        final GitHubRepoViewHolder viewHolder = new GitHubRepoViewHolder(itemGithubRepoBinding);
        itemGithubRepoBinding.getRoot().setTag(viewHolder);
        return itemGithubRepoBinding;
    }

    private  class GitHubRepoViewHolder {

        public ItemGithubRepoBinding repoBinding;



        public GitHubRepoViewHolder(ItemGithubRepoBinding repoBinding) {
            this.repoBinding=repoBinding;

        }


    }
}