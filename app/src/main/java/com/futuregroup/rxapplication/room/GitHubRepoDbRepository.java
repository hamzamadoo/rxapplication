package com.futuregroup.rxapplication.room;

import com.futuregroup.rxapplication.App;
import com.futuregroup.rxapplication.GitHubRepo;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class GitHubRepoDbRepository {

    private static GitHubRepoDbRepository gitHubRepoDbRepository;

    public static GitHubRepoDbRepository getRepository() {
        synchronized ("") {
            if (gitHubRepoDbRepository == null)
                return gitHubRepoDbRepository = new GitHubRepoDbRepository();
            else
                return gitHubRepoDbRepository;
        }
    }


    public Observable<List<Long>> insertAllRepos(List<GitHubRepo> repos) {

        return Observable.create(e -> {


            e.onNext(App.getInstance().getDB().gitHubRepoDao().insertAllGitHubRepos(repos));
            e.onComplete();

        });
    }

    public Observable<Integer> deleteAllRepos(List<GitHubRepo> repos){

        return Observable.fromCallable(() -> App.getInstance().getDB().gitHubRepoDao().deleteAllGitHubRepos(repos));
    }

    public Observable<List<GitHubRepo>> getAllRepos(){

        return Observable.fromCallable(() -> App.getInstance().getDB().gitHubRepoDao().getAllGitHubRepos());
    }


    public <T> Disposable performDBOperation(Observable<T> call, Consumer<T> success, Consumer<Throwable> error) {



            return call.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, error);



    }
}
