package com.futuregroup.rxapplication.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.futuregroup.rxapplication.GitHubRepo;


@Database(entities = {GitHubRepo.class},version = 1,exportSchema = false)
public abstract class GitHubRepoDatabase extends RoomDatabase{

    public abstract GitHubRepoDao gitHubRepoDao();
}
