package com.futuregroup.rxapplication.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.futuregroup.rxapplication.GitHubRepo;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface GitHubRepoDao {

    @Insert( onConflict = REPLACE)
    List<Long> insertAllGitHubRepos(List<GitHubRepo> repos);

    @Query("SELECT * FROM GitHubRepo")
    List<GitHubRepo> getAllGitHubRepos();

    @Delete
    int deleteAllGitHubRepos(List<GitHubRepo> repos);
}
