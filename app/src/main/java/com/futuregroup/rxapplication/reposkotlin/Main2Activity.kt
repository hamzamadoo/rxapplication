//package com.futuregroup.rxapplication.reposkotlin
//
//import android.os.Bundle
//import android.util.Log
//import com.futuregroup.rxapplication.BaseActivity
//import com.futuregroup.rxapplication.GitHubRepo
//import com.futuregroup.rxapplication.GitHubRepoAdapter
//import com.futuregroup.rxapplication.R
//import com.futuregroup.rxapplication.databinding.ActivityMain2Binding
//import com.futuregroup.rxapplication.model.User
//import com.futuregroup.rxapplication.repos.MainActivity
//import com.futuregroup.rxapplication.repos.MainNavigator
//import com.futuregroup.rxapplication.repos.MainViewModel
//
//class Main2Activity : BaseActivity<ActivityMain2Binding, MainViewModel>(), MainNavigator {
//    private val TAG = MainActivity::class.java.simpleName
//    private var adapter: GitHubRepoAdapter? = null
//    override fun handleError(throwable: Throwable?) {
//        if (throwable != null) {
//            throwable.printStackTrace()
//        }
//        Log.d(TAG, "In onError()")
//    }
//
//    override fun onReposFetched(repos: MutableList<GitHubRepo>?) {
//
//        Log.d(TAG, "In onReposFetched()")
//        adapter?.setGitHubRepos(repos)
//    }
//
//    override fun onUserNameError() {
//        viewDataBinding.tilUserName.error = "Enter User Name"
//    }
//
//    override fun onBinding() {
//        adapter = GitHubRepoAdapter(this)
//        viewDataBinding.listViewRepos.adapter = adapter
//        viewDataBinding.viewModel = getmViewModel()
//        viewDataBinding.user = User()
//        check(1)
//
//    }
//
//    override fun getViewModel(): MainViewModel {
//        return MainViewModel(this, this)
//    }
//
//    override fun getLayoutId(): Int {
//        return R.layout.activity_main2
//    }
//
//
//    fun check(value: Any?) {
////        val param:String ="Amit"
//        when (value) {
//            is Int -> println("Input parameter is Integer")
//            is String -> println("Input parameter is String")
//            else -> println("Other Input paramter ")
//
//
//        }
//
//
//        val items = listOf("apple", "banana", "kiwifruit")
//        for (index in items.indices) {
//            println("item at $index is ${items[index]}")
//        }
//    }
//
//    fun describe(obj: Any): String =
//            when (obj) {
//                1 -> "One"
//                "Hello" -> "Greeting"
//                is Long -> "Long"
//                !is String -> "Not a string"
//                else -> "Unknown"
//            }
//
//    fun describe1(obj: Any): String {
//        when (obj) {
//            1 -> return "One"
//            "Hello" -> return "Greeting"
//            is Long -> return "Long"
//            !is String -> return "Not a string"
//            else -> return "Unknown"
//        }
//    }
//
//
//}
