package com.futuregroup.rxapplication;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GitHubClient {

    private static final String GITHUB_BASE_URL = "https://api.github.com/";

//    private static final String GITHUB_BASE_URL = "http://dev.felindia.in/";
    private static Retrofit retrofit = null;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


    public static <T> T getClientInterface(Class<T> t) {
        final Gson gson =
                new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(GITHUB_BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .client(getHttpClient( Credentials.basic("fel-edre-web","gfh384ARY@64")))
                    .build();
        }
        return retrofit.create(t);
    }

    public static OkHttpClient getHttpClient(final String authToken){


            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(authToken);

//            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

               return httpClient.build();

//            }



    }

}