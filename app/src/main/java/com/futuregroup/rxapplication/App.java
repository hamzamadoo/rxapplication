package com.futuregroup.rxapplication;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.gsonparserfactory.GsonParserFactory;
import com.futuregroup.rxapplication.room.GitHubRepoDatabase;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 4/26/2018.
 */



public class App extends Application {

    private static final String DATABASE_NAME = "gitHub_repo_db";
    private GitHubRepoDatabase gitHubRepoDatabase;
    private static App app;
    @Override
    public void onCreate() {
        super.onCreate();

        if(app==null)
            app=this;

        AndroidNetworking.initialize(getApplicationContext());
        final Gson gson =
                new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

        AndroidNetworking.setParserFactory(new GsonParserFactory(gson));


        if(gitHubRepoDatabase==null)
        gitHubRepoDatabase = Room.databaseBuilder(getApplicationContext(),
                GitHubRepoDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();


    }

    public static App getInstance(){

        return app;
    }

    public GitHubRepoDatabase getDB(){
        return this.gitHubRepoDatabase;
    }
}
