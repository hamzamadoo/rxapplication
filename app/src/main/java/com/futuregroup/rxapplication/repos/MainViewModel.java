package com.futuregroup.rxapplication.repos;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.futuregroup.rxapplication.BaseViewModel;
import com.futuregroup.rxapplication.GitHubClient;
import com.futuregroup.rxapplication.GitHubRepo;
import com.futuregroup.rxapplication.GitHubService;
import com.futuregroup.rxapplication.room.GitHubRepoDbRepository;
import com.futuregroup.rxapplication.utils.NetworkHelper;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static android.content.ContentValues.TAG;

/**
 * Created by user on 4/18/2018.
 */

public class MainViewModel extends BaseViewModel<MainNavigator> {

    Context mContext;
    private GitHubRepoDbRepository repository;

    public MainViewModel(MainNavigator n, Context context) {
        setNavigator(n);
        mContext = context;
        repository = GitHubRepoDbRepository.getRepository();
    }

    public void getRepos(String user,List repos) {
        if (!TextUtils.isEmpty(user)) {
            Date date1 = Calendar.getInstance().getTime();

            getCompositeDisposable().add(NetworkHelper.create(mContext).callWebService(GitHubClient.getClientInterface(GitHubService.class).getStarredRepositories(user), gitHubRepos -> {
                Date date2 = Calendar.getInstance().getTime();
                Log.e("time_taken", String.valueOf(date2.getTime() - date1.getTime()));
                Log.d(TAG, "In onNext()");

                Log.e("repos",new Gson().toJson(gitHubRepos.get(gitHubRepos.size()-1)));

                getCompositeDisposable().add(repository.performDBOperation(repository.deleteAllRepos(repos), aLong -> {
                    getCompositeDisposable().add(repository.performDBOperation(repository.insertAllRepos(gitHubRepos), bLong -> {

                        if (bLong.get(bLong.size()-1) > 0)
                            getNavigator().onReposInsertedToDB(bLong.get(bLong.size()-1));

                    }, throwable -> {
                        throwable.printStackTrace();
                        getNavigator().handleError(throwable);
                    }));
                }, throwable -> {
                    throwable.printStackTrace();
                    getNavigator().handleError(throwable);
                }));


//                getNavigator().onReposFetched(gitHubRepos);
            }, throwable -> {
                throwable.printStackTrace();
                getNavigator().handleError(throwable);
                Log.d(TAG, "In onError()");
            }));


        } else {
            getNavigator().onUserNameError();
        }

//        http://dev.felindia.in/easyday/Apks/crm/crm-debug.apk
    }

    public void fetchRepos(){

        getCompositeDisposable().add(repository.performDBOperation(repository.getAllRepos(), repos -> {

            getNavigator().onReposFetched(repos);

        }, throwable -> {
            throwable.printStackTrace();
            getNavigator().handleError(throwable);
            Log.d(TAG, "In onError()");
        }));
    }

    private void getReposWithFastNetworkingLib(String user) {
        Date date1 = Calendar.getInstance().getTime();
        AndroidNetworking.get("https://api.github.com/users/{user}/starred")
                .addPathParameter("user", user)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObjectList(GitHubRepo.class, new ParsedRequestListener<List<GitHubRepo>>() {
                    @Override
                    public void onResponse(List<GitHubRepo> repos) {
                        Date date2 = Calendar.getInstance().getTime();
                        Log.e("time_taken", String.valueOf(getTimeDiffInSec(date2.getTime() - date1.getTime())));

                        // do anything with response

                    }

                    @Override
                    public void onError(ANError anError) {
                        // handle error
                    }
                });
    }


    private long getTimeDiffInSec(long diff) {
        return diff / 1000 % 60;
    }


}