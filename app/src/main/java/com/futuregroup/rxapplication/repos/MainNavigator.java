package com.futuregroup.rxapplication.repos;

import com.futuregroup.rxapplication.GitHubRepo;

import java.util.ArrayList;
import java.util.List;

public interface MainNavigator {

        void handleError(Throwable throwable);

        void onReposFetched(List<GitHubRepo> repos);

        void onReposInsertedToDB(Long cnt);

        void onUserNameError();

    }
