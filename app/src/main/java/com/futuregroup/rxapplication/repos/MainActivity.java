package com.futuregroup.rxapplication.repos;

import android.annotation.TargetApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.futuregroup.rxapplication.BaseActivity;
import com.futuregroup.rxapplication.GitHubClient;
import com.futuregroup.rxapplication.GitHubRepo;
import com.futuregroup.rxapplication.GitHubRepoAdapter;
import com.futuregroup.rxapplication.R;
import com.futuregroup.rxapplication.databinding.ActivityMainBinding;
import com.futuregroup.rxapplication.model.User;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends BaseActivity<ActivityMainBinding,MainViewModel> implements MainNavigator {
    private static final String TAG = MainActivity.class.getSimpleName();
    private GitHubRepoAdapter adapter;



    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        return new MainViewModel(this,this);
    }

   /* @Override
    public MainNavigator getNavigator() {
        return this;
    }*/

    @Override
    public void onBinding() {
        adapter = new GitHubRepoAdapter(this);
        getViewDataBinding().listViewRepos.setAdapter(adapter);
        getViewDataBinding().setViewModel(getmViewModel());
        getViewDataBinding().setUser(new User());
        getViewDataBinding().setRepos(adapter.getGitHubRepos());


    }









    @Override
    public void handleError(Throwable throwable) {
        throwable.printStackTrace();
        Log.d(TAG, "In onError()");
    }

    @Override
    public void onReposFetched(List<GitHubRepo> repos) {
        Log.d(TAG, "In onReposFetched()");
        adapter.setGitHubRepos(repos);
    }

    @Override
    public void onReposInsertedToDB(Long cnt) {
        Toast.makeText(this,cnt+" records inserted",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserNameError() {
        getViewDataBinding().tilUserName.setError("Enter User Name");
    }


}
