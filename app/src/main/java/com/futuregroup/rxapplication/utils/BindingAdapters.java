package com.futuregroup.rxapplication.utils;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;

/**
 * Created by user on 4/20/2018.
 */

public class BindingAdapters {

    @BindingAdapter({"userName"})
    public static void hideError(TextInputLayout view, String text) {

        if (!TextUtils.isEmpty(text))
            view.setError(null);
    }
}
