package com.futuregroup.rxapplication.utils;

import android.content.Context;


import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by hamza.
 */

public class NetworkHelper {

    private static final String TAG = NetworkHelper.class.getSimpleName();

    private Context mContext;



    private static NetworkHelper networkHelper;

    private NetworkHelper(Context mContext){
        this.mContext = mContext;
    }

    public static NetworkHelper create(Context context) {
        if (networkHelper == null) {
            networkHelper = new NetworkHelper(context);
        }
        return networkHelper;
    }


    public <T> Disposable callWebService(Observable<T> call, Consumer<T> success,Consumer<Throwable> error) {

        if (Utils.getInstance().isNetworkAvailable(mContext)) {

             return call.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, error);


        } else {
            Utils.getInstance().showMessage(mContext,"No Internet");
            return null;
        }
    }





}
